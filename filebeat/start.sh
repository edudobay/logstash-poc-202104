#!/usr/bin/env bash

set -o nounset -o errexit -o pipefail

root=$(dirname "$0")
configFile=$root/config.yml

appRoot=$root/filebeat-7.12.0-linux-x86_64

exec sudo "$appRoot/filebeat" -c "$configFile" -e --strict.perms=false
